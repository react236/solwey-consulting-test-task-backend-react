import { AnyAction } from 'redux'
import { iItem, iOrder, iUser } from '../interfaces'

import {
    ADD_TO_CART,
    CLEAR_ITEMS,
    CLEAR_CART,
    CLEAR_USER,
    CLEAR_USERS,
    DELETE_ITEM,
    DELETE_USER,
    INIT_SHOP,
    REMOVE_FROM_CART_BY_ONE,
    SET_DATA_LOADING,
    STORE_ITEMS,
    STORE_ORDERS,
    STORE_USER,
    STORE_USERS,
} from './constants'

export function initShop(): AnyAction {
    // store.dispatch(initMinesLeft())
    // store.dispatch(setGameWon(false))
    return { type: INIT_SHOP }
}

export function addToCart(id: number, count: number): AnyAction {
    return { type: ADD_TO_CART, payload: { id, count } }
}

export function removeFromCartByOnee(id: number): AnyAction {
    return { type: REMOVE_FROM_CART_BY_ONE, payload: id }
}

export function clearCart(): AnyAction {
    return { type: CLEAR_CART }
}

export function storeItems(items: iItem[]): AnyAction {
    return { type: STORE_ITEMS, payload: items }
}

export function clearItems(): AnyAction {
    return { type: CLEAR_ITEMS }
}

export function deleteItem(id: number): AnyAction {
    return { type: DELETE_ITEM, payload: id }
}

export function storeOrders(orders: iOrder[]): AnyAction {
    return { type: STORE_ORDERS, payload: orders }
}

export function storeUser(user: iUser): AnyAction {
    return { type: STORE_USER, payload: user }
}

export function clearUser(): AnyAction {
    return { type: CLEAR_USER }
}

export function deleteUser(id: number): AnyAction {
    return { type: DELETE_USER, payload: id }
}

export function storeUsers(users: iUser[]): AnyAction {
    return { type: STORE_USERS, payload: users }
}

export function clearUsers(): AnyAction {
    return { type: CLEAR_USERS }
}

export function setDataLoading(loading: boolean): AnyAction {
    return { type: SET_DATA_LOADING, payload: loading }
}
