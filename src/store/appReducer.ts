import { AnyAction } from "redux"

import {
    ADD_TO_CART,
    CLEAR_CART,
    CLEAR_ITEMS,
    CLEAR_USER,
    CLEAR_USERS,
    DELETE_ITEM,
    DELETE_USER,
    INIT_SHOP,
    REMOVE_FROM_CART_BY_ONE,
    SET_DATA_LOADING,
    STORE_ITEMS,
    STORE_ORDERS,
    STORE_USER,
    STORE_USERS,
} from './constants'

import { iCartItem, iItem, iUser } from "../interfaces"

interface iStoreState {
    cart: iCartItem[]
    items: iItem[]
    users: iUser[]
    user: iUser
    orders: []
    dataLoading: boolean
}

const initShop = () => {

    return null
}

const initialState: iStoreState = {
    cart: [],
    items: [],
    users: [],
    user: {
        id: 0,
        first_name: '',
        last_name: '',
        email: '',
        token: '',
        refreshToken: '',
        role: '',
    },
    orders: [],
    dataLoading: false,
}

export const appReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        // Инициализация магазина
        case INIT_SHOP: { 
            return state
        }
        
        // Добавляем товар в корзинку
        case ADD_TO_CART: { 
            const newCart = Object.assign([], state.cart)
            // Проверяем есть ли уже такой ID в корзине
            let index = newCart.findIndex((item) => item.id === action.payload.id);
            if (index === -1) {
                newCart.push({ id: action.payload.id, count: action.payload.count})
            } else {
                const newCount = newCart[index].count + action.payload.count
                newCart[index].count = newCount
            }

            return { ...state, cart: newCart }
        }

        // Удаляем товар из корзины по одному
        case REMOVE_FROM_CART_BY_ONE: {
            const newState = Object.assign({}, state)
            // Проверяем есть ли уже такой ID в корзине
            let index = newState.cart.findIndex((item) => item.id === action.payload.id);
            if (index === -1) {
                return state
            } else {
                let newCount = newState.cart[index].count - 1
                if (newCount < 1) newCount = 1
                newState.cart[index].count = newCount
            }

            return newState
        }

        // Удаляем товар из корзины полностью
        case CLEAR_CART: {
            const newState = Object.assign({}, state)
            newState.cart = []

            return newState
        }

        // Сохраняем скачанные товары
        case STORE_ITEMS: {
            const newState = Object.assign({}, state)
            newState.items = action.payload
            
            return newState
        }
        
        // Удаляем товары
        case CLEAR_ITEMS: {
            const newState = Object.assign({}, state)
            newState.items = []
            
            return newState
        }
        
        // Удаляем один товар
        case DELETE_ITEM: {
            const newItems = Object.assign([], state.items)

            const itemIndex = newItems.findIndex(item => item.id === action.payload)

            if (itemIndex >= 0) {
                newItems.splice(itemIndex, 1)
            }

            return { ...state, items: newItems }
        }

        // Сохраняем данные пользователя
        case STORE_USER: {
            const newState = Object.assign({}, state)
            newState.user = {
                id: action.payload.id,
                first_name: action.payload.first_name,
                last_name: action.payload.last_name,
                email: action.payload.email,
                token: action.payload.token,
                refreshToken: action.payload.refreshToken,
                role: action.payload.role,
            }
            
            return newState
        }
        
        // Удаляем данные пользователя
        case CLEAR_USER: {
            const newState = Object.assign({}, state)
            newState.user = {
                id: 0,
                first_name: '',
                last_name: '',
                email: '',
                token: '',
                refreshToken: '',
                role: '',
            }
            
            return newState
        }

        // Сохраняем данные пользователей
        case STORE_USERS: {
            const newState = Object.assign({}, state)
            newState.users = action.payload
            
            return newState
        }
        
        // Удаляем данные пользователей
        case CLEAR_USERS: {
            const newState = Object.assign({}, state)
            newState.users = []
            
            return newState
        }

        // Удаляем одиного пользователя
        case DELETE_USER: {
            const newUsers = Object.assign([], state.users)

            const userIndex = newUsers.findIndex(user => user.id === action.payload)

            if (userIndex >= 0) {
                newUsers.splice(userIndex, 1)
            }

            return { ...state, users: newUsers }
        }

        // Сохраняем ордера
        case STORE_ORDERS: {
            const newState = Object.assign({}, state)
            newState.orders = action.payload
            
            return newState
        }

        // Флаг загрузки данных
        case SET_DATA_LOADING: {
            return { ...state, dataLoading: action.payload }
        }

        default: { return state }
    }
}
