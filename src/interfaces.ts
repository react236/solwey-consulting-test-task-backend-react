export interface iItem {
    id: number
    name: string
    description: string
    price: number
}

export interface iCartItem {
    id: number,
    count: number,
}

export type tRole = '' | 'guest' | 'user' | 'admin'

export interface iUser {
    id: number
    first_name: string
    last_name: string
    email: string
    token: string
    refreshToken: string
    role: tRole
}

export interface iOrderDetails {
    item_id: number,
    quantity: number
}

export interface iOrder {
    id: number,
    user_id: number,
    amount: number,
    email: string,
    details: iOrderDetails[]
}
