import axios from "axios"

import { store } from '../store/store'
import { setDataLoading } from "../store/actions"
import { errorMessage, errorToast, noData } from "./helpers"
import { options } from './options'

// let backendURL = 'https://solweyconsultingtesttaskbackend.onrender.com'
let backendURL = 'https://mysite-i41w.onrender.com'
if (import.meta.env.MODE === 'development') {
    backendURL = 'http://localhost:3000'
}

const request = async (method: any, url: string, body: any, token = '', spinner: boolean = false) => {
    const fullUrl = backendURL + url

    let newToken = token
    if (token === '') {
        newToken = store.getState().app.user['token']
    }
    options.headers['Authorization'] = `Bearer ${newToken}`

    let result

    try {
        if (spinner) store.dispatch(setDataLoading(true))
        if (body) {
            result = await method(fullUrl, body, options)
        } else {
            result = await method(fullUrl, options)
        }
    } catch (e: any) {
        result = errorMessage(e)
    } finally {
        if (spinner) store.dispatch(setDataLoading(false))
    }
    
    if (result) {
        if ('data' in result) {

            if (typeof result.data === 'object' && 'error' in result.data) {
                errorToast(result.data.error)
            }

            if (typeof result.data === 'string') {
                return {}
            }
            
            return result.data
        }
    }
    
    return noData
}

export const getRequest = async (url: string, token = '', spinner: boolean = false) => {
    const result = await request(axios.get, url, null, token, spinner)
    return result
}

export const postRequest = async (url: string, body: any, token: string = '', spinner: boolean = false) => {
    const result = await request(axios.post, url, body, token, spinner)
    return result
}

export const patchRequest = async (url: string, body: any, token: string = '', spinner: boolean = false) => {
    const fullUrl = backendURL + url
    // const options = {
    //     headers: {
    //         'Content-Type': 'application/json'
    //     }
    // }

    let newToken = token
    if (token === '') {
        newToken = store.getState().app.user['token']
    }
    options.headers['Authorization'] = `Bearer ${newToken}`

    let result: any

    try {
        if (spinner) store.dispatch(setDataLoading(true))
        result = await axios.patch(fullUrl, body, options)
    } catch (e: any) {
        result = errorMessage(e)
    } finally {
        if (spinner) store.dispatch(setDataLoading(false))
    }
    
    
    if (result) {
        if ('data' in result) {
            if ('error' in result.data) {
                errorToast(result.data.error)
            }

            return result.data
        }
    }
    
    return noData
}

export const deleteRequest = async (url: string, token: string = '', spinner: boolean = false) => {
    const fullUrl = backendURL + url

    let newToken = token
    if (token === '') {
        newToken = store.getState().app.user['token']
    }
    options.headers['Authorization'] = `Bearer ${newToken}`

    let result: any

    try {
        if (spinner) store.dispatch(setDataLoading(true))
        result = await axios.delete(fullUrl, options)
    } catch (e: any) {
        result = errorMessage(e)
    } finally {
        if (spinner) store.dispatch(setDataLoading(false))
    }
    
    
    if (result) {
        if ('data' in result) {
            if ('error' in result.data) {
                errorToast(result.data.error)
            }

            return result.data
        }
    }
    
    return noData
}
