import { getRequest, postRequest, patchRequest, deleteRequest } from './network'
import { filterName } from './helpers'

export const itemsRequest = async (q: string | null) => {
    const filtered = filterName(q)
    let url = '/items'
    if (filtered !== '') url = `${url}?q=${filtered}`

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const updateItemRequest = async (id: string, data: any) => {
    const url = `/items/${id}`
    const item = {
        item: data
    }
    const result = await patchRequest(url, item, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const newItemRequest = async (data: any) => {
    const url = '/items'
    const item = {
        item: data
    }
    const result = await postRequest(url, item, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const deleteItemRequest = async (id: string) => {
    const url = `/items/${id}`

    const result = await deleteRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}
