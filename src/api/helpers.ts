import { toast } from 'react-toastify'

// Убираем лишние символы из названия
export const filterName = (name: string | null): string => {
    if (name === '' || name === null) {
        return ''
    }
    
    const forbidden = '!@#$%^&*()_+={}|[]\\:";\'<>?,./~`'

    let filtered = name.trim().slice(0, 30).split('').filter(item => !forbidden.includes(item)).join('')
    return filtered
}

// Убираем лишние символы
export const filterEmail = (name: string | null): string => {
    if (name === '' || name === null) {
        return ''
    }
    
    const forbidden = '!$%^&*()={}|[]\\:";\'<>?,/~`'

    let filtered = name.trim().slice(0, 30).split('').filter(item => !forbidden.includes(item)).join('')
    return filtered
}

export const errorToast = (result: any) => {
    toast.error(result, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: false,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
    });
}

export const errorMessage = (e: any) => {
    let eMessage: any

    if (('response' in e) && ('data' in e.response) && ('error' in e.response.data)) {
        eMessage = { data: { error: e.response.data.error } }
    } else {
        eMessage = { data: { error: e.message } }
    }

    return eMessage
} 

export const noData = { error: 'No Data' }
