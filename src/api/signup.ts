import { postRequest } from './network'
import { filterEmail } from './helpers'

export const signupRequest = async (email: string, password: string) => {
    const emailFiltered = filterEmail(email)
    let url = '/users/tokens/sign_up'
    
    if (emailFiltered === '' || password === '') {
        return []
    }

    const result = await postRequest(url, {
        email: emailFiltered,
        password,
        role: 'user',
    }, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}
