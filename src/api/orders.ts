import { getRequest, postRequest } from './network'

import { iCartItem } from '../interfaces'

export const newOrderRequest = async (orderData: iCartItem[]) => {
    const url = '/orders'

    const orderObject = {
        order: orderData
    }

    const result = await postRequest(url, orderObject, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const ordersRequest = async () => {
    const url = '/orders'

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}
