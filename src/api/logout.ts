import { postRequest } from './network'

export const logoutRequest = async (token: string) => {
    let url = '/users/tokens/revoke'
    
    const result = await postRequest(url, {}, token, true)

    if ( !('error' in result) ) {
        return result
    }

    return {}
}
