import { postRequest } from './network'
import { filterEmail } from './helpers'

export const signinRequest = async (email: string, password: string) => {
    const emailFiltered = filterEmail(email)
    let url = '/users/tokens/sign_in'

    if (emailFiltered === '' || password === '') {
        return {}
    }

    const result = await postRequest(url, {
        email: emailFiltered,
        password
    }, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return {}
}
