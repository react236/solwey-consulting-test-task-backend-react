import { getRequest, postRequest, patchRequest, deleteRequest } from './network'

export const userRequest = async (id, token = '') => {
    let url = `/users/${id}`

    const result = await getRequest(url, token, true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const usersRequest = async () => {
    let url = '/users'

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const updateUserRequest = async (id: string, data: any, token: string = '') => {
    const url = `/users/${id}`
    const user = {
        user: data
    }
    const result = await patchRequest(url, user, token, true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const newUserRequest = async (data: any) => {
    const url = '/users'
    const user = {
        user: data
    }
    const result = await postRequest(url, user, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const deleteUserRequest = async (id: string) => {
    const url = `/users/${id}`

    const result = await deleteRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}
