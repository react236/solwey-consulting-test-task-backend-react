import { createBrowserRouter, RouterProvider } from "react-router-dom"
import { Provider } from "react-redux"

import Root from "./routes/root"
import ErrorPage from "./components/errorPage/errorPage"
import Items from "./routes/items"
import Cart from "./routes/cart"
import Orders from "./routes/orders"
import Order from "./routes/order"
import ItemCard from "./routes/itemCard"
import EditItem from "./routes/editItem"
import ItemsList from "./routes/itemsList"
import SignUp from './routes/signUp'
import SignIn from './routes/signIn'
import Logout from './routes/logout'
import UsersList from './routes/usersList'
import UserProfile from './routes/userProfile'
import EditUser from './routes/editUser'

import { loader as itemsLoader } from './components/items/Items'
import { loader as ordersLoader } from './components/orders/Orders'
import { loader as itemCardLoader } from './components/itemCard/ItemCard'
import { loader as editItemLoader } from './components/editItem/EditItem'
import { loader as itemsListLoader } from './components/itemsList/ItemsList'
import { loader as usersListLoader } from './components/usersList/UsersList'
import { loader as editUserLoader } from './components/editUser/EditUser'
import { loader as userProfileLoader } from './components/userProfile/UserProfile'

import { action as cartAction } from "./components/cart/Cart"
import { action as editItemAction } from "./components/editItem/EditItem"
import { action as signUpAction } from "./components/signUp/SignUp"
import { action as signInAction } from "./components/signIn/SignIn"
import { action as logoutAction } from "./components/logout/Logout"
import { action as editUserAction } from "./components/editUser/EditUser"

import { Spinner } from './components/spinner/Spinner'
import { store } from './store/store'

const router = createBrowserRouter([
    {
        path: "/",
        element: <Root />,
        errorElement: <ErrorPage />,
        children: [
            {
                errorElement: <ErrorPage />,
                children: [
                    {
                        index: true,
                        element: <Items />,
                        loader: itemsLoader,
                    },
                    {
                        path: "cart",
                        element: <Cart />,
                        action: cartAction,
                    },
                    {
                        path: "orders",
                        element: <Orders />,
                        loader: ordersLoader,
                    },
                    {
                        path: "order/:id",
                        element: <Order />,
                    },
                    {
                        path: "items/:id",
                        element: <ItemCard />,
                        loader: itemCardLoader,
                    },
                    {
                        path: "items/:id/edit",
                        element: <EditItem />,
                        loader: editItemLoader,
                        action: editItemAction,
                    },
                    {
                        path: "items/list",
                        element: <ItemsList />,
                        loader: itemsListLoader,
                    },
                    {
                        path: "signup",
                        element: <SignUp />,
                        action: signUpAction,
                    },
                    {
                        path: "signin",
                        element: <SignIn />,
                        action: signInAction,
                    },
                    {
                        path: "logout",
                        element: <Logout />,
                        action: logoutAction,
                    },
                    {
                        path: "/users/list",
                        element: <UsersList />,
                        loader: usersListLoader,
                    },
                    {
                        path: "users/:id",
                        element: <UserProfile />,
                        loader: userProfileLoader,
                    },
                    {
                        path: "users/:id/edit",
                        element: <EditUser />,
                        loader: editUserLoader,
                        action: editUserAction,
                    },
                    {
                        path: "/users/:id/delete",
                    },
                ],
            },
        ],
    },
]);

const App = () => {
    return (
        <Provider store={store}>
            <RouterProvider
                router={router}
                fallbackElement = {(
                    <div style={{ position: 'relative' }}>
                        <Spinner enable={true} />
                    </div>
                )}
            />
        </Provider>
    )
}

export default App

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
