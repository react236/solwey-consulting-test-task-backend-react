/**
 * Страница одного ордера
 */
import React from 'react'

import { Link, useParams, useNavigate } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'

import { ordersRequest } from '../../api'
import { store } from '../../store/store'
import { storeOrders } from '../../store/actions'
import { format } from '../../utils'
import { iOrder, iOrderDetails } from '../../interfaces'

import styles from './Order.module.css'

export async function loader({ request }) {
    let orders = store.getState().app['orders']
    if (orders.length === 0) {
        orders = await ordersRequest()
        store.dispatch(storeOrders(orders))
    }

    return { orders };
}

export const Order = () => {
    const orders: iOrder[] = useAppSelector(store => store.app['orders'])
    const items = useAppSelector(store => store.app['items'])
    const params = useParams()
    const navigate = useNavigate()

    const handleBack = () => {
        navigate(-1)
    }

    const order = orders.find((item: iOrder) => item.id === Number(params.id))
    if ( !order ) {
        return <p>Заказ не найден</p>
    }

    return (
        <div className={styles.container}>
            <h1>Заказ №{params.id}</h1>
            <table>
                <thead>
                    <tr>
                        <th>Наименование</th>
                        <th>Количество</th>
                        <th>Цена</th>
                        <th>Сумма</th>
                    </tr>
                </thead>
                <tbody>
                    {order.details.map((orderItem: iOrderDetails) => {
                        const item = items.find((item2) => item2.id === orderItem.item_id)
                        const sum = orderItem.quantity * item!.price

                        return (
                            <tr key={orderItem.item_id}>
                                <td className={styles.name}>{item!.name}</td>
                                <td className={styles.count}>{orderItem.quantity}</td>
                                <td className={styles.amount}>{format(item!.price)}</td>
                                <td className={styles.amount}><Link to={`/order/${orderItem.item_id}`}>{format(sum)}</Link></td>
                            </tr>
                        )
                    })}
                </tbody>
                <tfoot>
                    <tr>
                        <th scope='row' className={styles.total}>Итого</th>
                        <td></td>
                        <td></td>
                        <th className={styles.amount}>{format(order.amount)}</th>
                    </tr>
                </tfoot>
            </table>
            <button
                className={styles.button}
                onClick={handleBack}
            >Назад</button>
        </div>
    )
}
