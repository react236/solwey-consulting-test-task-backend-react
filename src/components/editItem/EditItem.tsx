/**
 * Редактирование товара
 */
import React from 'react'
import { Form, useParams, useNavigate, redirect } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'
import { Access } from '../access/Access'

import { itemsRequest, updateItemRequest, newItemRequest } from '../../api'
import { store } from '../../store/store'
import { clearItems, storeItems } from '../../store/actions'
import { iItem } from '../../interfaces'

import styles from './EditItem.module.css'

export async function loader({ request }) {
    let items = store.getState().app['items']
    if (items.length === 0) {
        items = await itemsRequest('')
        store.dispatch(storeItems(items))
    }

    return { items };
}

export async function action({ request, params }) {
    const formData = await request.formData();
    const updates = Object.fromEntries(formData);
    
    if (params.id === 'new') {
        const result = await newItemRequest(updates);
        store.dispatch(clearItems())
        return redirect(`/items/list`)
    }

    const result = await updateItemRequest(params.id, updates);
    store.dispatch(clearItems())
    return redirect(`/items/${params.id}`)
}

export const EditItem = () => {
    const items = useAppSelector(store => store.app['items'])
    const params = useParams()
    const navigate = useNavigate()

    let item: iItem
    if (params.id !== 'new') {
        item = items.find((item) => item.id === Number(params.id))
        if ( !item ) {
            return <p>Товар не найден</p>
        }
    }

    const handleBack = () => {
        navigate(-1)
    }

    return (
        <Access role='admin' to='/'>
            <Form method="post">
                <div className={styles.formContainer}>
                    <h1>Редактирование товара</h1>
                    <label>
                        <span>Наименование</span><br />
                        <input
                            className={styles.name}
                            defaultValue={params.id !== 'new' ? item.name : ''}
                            placeholder='Наименование'
                            type='text'
                            name='name'
                        />
                    </label>
                    <label>
                        <span>Цена</span><br />
                        <input 
                            className={styles.price}
                            defaultValue={params.id !== 'new' ? item.price : ''}
                            placeholder='Цена'
                            type='text'
                            name='price'
                        />
                    </label>
                    <label>
                        <span>Описание</span><br />
                        <textarea 
                            className={styles.description}
                            defaultValue={params.id !== 'new' ? item.description : ''}
                            rows={6}
                            name='description'
                        />
                    </label>
                    <p>
                        <button 
                            className={styles.button}
                            type='submit'
                        >Сохранить</button>
                        <button
                            className={styles.button}
                            type='button'
                            onClick={handleBack}
                        >Отмена</button>
                    </p>
                </div>
            </Form>
        </Access>
    )
}
