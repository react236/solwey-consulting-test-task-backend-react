import React from 'react'
import { useAppSelector } from '../../store/hooks'
import { iCartItem } from '../../interfaces'
import styles from './CartBadge.module.css'

function cartItemsCount(cart: iCartItem[]): number {
    let count = cart.reduce((acc, current) => acc + current.count, 0)
    
    return count
}

export const CartBadge = () => {
    const count = useAppSelector(store => cartItemsCount(store.app['cart']))

    return (
        <div key={count} className={styles.container}>{count === 0 ? '' : count}</div>
    )
}
