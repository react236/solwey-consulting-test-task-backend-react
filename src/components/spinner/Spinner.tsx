import { SpinnerCircularFixed } from 'spinners-react'

interface iProps {
    enable: boolean
}

export const Spinner: React.FC<iProps> = ({ enable }) => {
    return (
        <SpinnerCircularFixed
            size={90} 
            thickness={110}
            speed={130}
            color="#36ad47"
            secondaryColor="rgba(0, 0, 0, 0.24)"
            style={{
                position: 'absolute',
                left: '50%',
                top: '320px',
                transform: 'translateX(-50%)',
            }}
            enabled = {enable}
        />
    )
}
