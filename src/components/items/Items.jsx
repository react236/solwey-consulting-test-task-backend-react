/**
 * Список товаров на главной
 */
import { Form, useLoaderData } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks';

import { Item } from '../item/Item'

import { store } from "../../store/store";
import { storeItems } from "../../store/actions";
import { itemsRequest } from '../../api/index'

import styles from './Items.module.css'

export async function loader({ request }) {
    const url = new URL(request.url)
    const q = url.searchParams.get("q")
    
    let items = store.getState().app['items']
    if (items.length === 0) items = await itemsRequest(q)
    if ('error' in items) {
        items = []
    }

    store.dispatch(storeItems(items))

    return { items, q }
}

const inCart = (item, cart) => {
    if (cart.findIndex(cartItem => cartItem.id === item.id) >= 0) {
        return true
    }

    return false
}

export const Items = () => {
    const { items, q } = useLoaderData()
    const cart = useAppSelector(store => store.app['cart'])
    
    return (
        <div className={styles.container}>
            <Form id='search-form' role='search'>
                <input
                    id="q"
                    className={styles.input}
                    defaultValue={q}
                    aria-label="Search contacts"
                    placeholder="Поиск"
                    type="search"
                    name="q"
                />
            </Form>
            <div className={styles.listContainer}>
                {items.map((item) => (
                    <Item
                        key={item.id}
                        id={item.id}
                        name={item.name}
                        description={item.description}
                        price={item.price}
                        inCart={inCart(item, cart)}
                    />
                ))}
            </div>
        </div>
    )
}
