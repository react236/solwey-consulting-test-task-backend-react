import React, { ReactElement } from 'react'
import { useNavigation } from 'react-router-dom'
import { Header } from '../header/Header'
import { Spinner } from '../spinner/Spinner'

import styles from './Layout.module.css'

interface iProps {
    children: ReactElement[]
}

export const Layout: React.FC<iProps> = ({ children }) => {
    const navigation = useNavigation()

    return (
        <div className={styles.container}>
            <Header />
            <div className={styles.data}>
                {children}
            </div>
            <Spinner enable={navigation.state === 'loading' || navigation.state === 'submitting'} />
        </div>
    )
}
