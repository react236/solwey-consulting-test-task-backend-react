import React from 'react'

import { Link, useNavigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { Access } from '../access/Access'

import { itemsRequest, deleteItemRequest } from '../../api'
import { store } from '../../store/store'
import { storeItems, deleteItem } from '../../store/actions'
import { format } from '../../utils'

import { iItem } from '../../interfaces'
import styles from './ItemsList.module.css'

export async function loader({ request }) {
    let items = store.getState().app['items']
    if (items.length === 0 ) {
        items = await itemsRequest('')
        store.dispatch(storeItems(items))
    }
    
    return { items };
}

export const ItemsList = () => {
    const items = useAppSelector(store => store.app['items'])
    const dispatch = useAppDispatch()
    const navigate = useNavigate()

    const handleNew = () => {
        navigate('/items/new/edit')
    }

    const handleDelete = async (id) => {
        await deleteItemRequest(id);
        // Удаляем товар со store
        dispatch(deleteItem(id))
    }

    return (
        <Access role='admin' to='/'>
            <div className={styles.container}>
                <h1>Список товаров</h1>
                {items.length > 0 ? (
                    <table>
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Наименование</th>
                                <th>Сумма</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {items.map((item: iItem) => {
                                return (
                                    <tr key={item.id}>
                                        <td className={styles.id}><Link to={`/items/${item.id}`}>{item.id}</Link></td>
                                        <td className={styles.name}><Link to={`/items/${item.id}`}>{item.name}</Link></td>
                                        <td className={styles.price}><Link to={`/items/${item.id}`}>{item.price}</Link></td>
                                        <td className={styles.delete} onClick={() => handleDelete(item.id)}>Удалить</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                ) : (
                    <p>Нет товаров</p>
                )}
                <button
                    className={styles.button}
                    onClick={handleNew}
                >
                    Создать
                </button>
            </div>
        </Access>
    )
}
