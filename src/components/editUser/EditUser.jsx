/**
 * Редактирование пользователя
 */
import { Form, useNavigate, redirect, useLoaderData } from 'react-router-dom'
import { Access } from '../access/Access'

import { userRequest, updateUserRequest } from '../../api'
import { store } from '../../store/store'
import { clearUsers, storeUser } from '../../store/actions'

import styles from './EditUser.module.css'

export async function loader({ params }) {
    let user = store.getState().app['user']

    if (user.id !== Number(params['id'])) {
        user = await userRequest(params['id'])
    }

    return { ... user }
}

export async function action({ request, params }) {
    const formData = await request.formData();
    let updates = Object.fromEntries(formData);

    await updateUserRequest(params.id, updates);
    store.dispatch(clearUsers())
    const currentUser = store.getState().app['user']
    if (currentUser.id === Number(params['id'])) {
        // Если редактируем самого себя, меняем также в store -> user
        store.dispatch(storeUser({
            ...currentUser,
            ...updates
        }))
    }
    return redirect(`/users/${params.id}`)
}

export const EditUser = () => {
    const user = useLoaderData()
    const navigate = useNavigate()

    const handleBack = () => {
        navigate(-1)
    }

    return (
        <Access role='user' to='/'>
            <Form method="post">
                <div className={styles.formContainer}>
                    <h1>Редактирование профиля пользователя {user.email}</h1>
                    <label>
                        <span>Имя</span><br />
                        <input
                            className={styles.name}
                            defaultValue={user.first_name}
                            placeholder='Имя'
                            type='text'
                            name='first_name'
                        />
                    </label>
                    <label>
                        <span>Фамилия</span><br />
                        <input 
                            className={styles.name}
                            defaultValue={user.last_name}
                            placeholder='Фамилия'
                            type='text'
                            name='last_name'
                        />
                    </label>
                    <label>
                        <span>Email</span><br />
                        <input 
                            className={styles.email}
                            defaultValue={user.email}
                            placeholder='Email'
                            type='text'
                            name='email'
                        />
                    </label>
                    <p>
                        <button 
                            className={styles.button}
                            type='submit'
                        >Сохранить</button>
                        <button
                            className={styles.button}
                            type='button'
                            onClick={handleBack}
                        >Отмена</button>
                    </p>
                </div>
            </Form>
        </Access>
    )
}
