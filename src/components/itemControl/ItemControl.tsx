import React, { useState } from 'react'
import { useAppDispatch } from '../../store/hooks'

import { addToCart } from '../../store/actions'

import styles from './ItemControl.module.css'

interface iProps {
    id: number
    inCart: boolean
}

export const ItemControl: React.FC<iProps> = ({ id, inCart }) => {
    const [count, setCount] = useState(1)
    const dispatch = useAppDispatch()

    const handleClickMinus = () => {
        if (count > 1) {
            setCount(count-1)
        }
    }

    const handleClickPlus = () => {
        setCount(count+1)
    }

    const handleClickAdd = () => {
        dispatch(addToCart(id, count))
    }

    return (
        <div className={styles.container}>
            <div className={styles.minus} onClick={handleClickMinus}>-</div>
            <span className={styles.count}>{count}</span>
            <div className={styles.plus} onClick={handleClickPlus}>+</div>
            <div className={styles.add} onClick={handleClickAdd}>
                {inCart ? 'В корзине' : 'Купить'}
            </div>
        </div>
    )
}
