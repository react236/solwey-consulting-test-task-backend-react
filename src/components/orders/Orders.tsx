/**
 * Список ордеров
 */
import React from 'react'

import { Link } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'

import { ordersRequest } from '../../api'
import { store } from '../../store/store'
import { storeOrders } from '../../store/actions'
import { format } from '../../utils'
import { iOrder } from '../../interfaces'

import styles from './Orders.module.css'

export async function loader({ request }) {
    const orders = await ordersRequest()
    if( 'error' in orders) {
        return {}
    }

    store.dispatch(storeOrders(orders))
    return { orders }
}

export const Orders = () => {
    const orders = useAppSelector(store => store.app['orders'])
    const role = useAppSelector(store => store.app.user['role'])

    return (
        <div className={styles.container}>
            <h1>Заказы</h1>
            {orders.length > 0 ? (
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Сумма</th>
                            {role === 'admin' && <th>Пользователь</th>}
                        </tr>
                    </thead>
                    <tbody>
                        {orders.map((item: iOrder) => {
                            return (
                                <tr key={item.id}>
                                    <td className={styles.id}><Link to={`/order/${item.id}`}>{item.id}</Link></td>
                                    <td className={styles.amount}><Link to={`/order/${item.id}`}>{format(item.amount)}</Link></td>
                                    {role === 'admin' && <td className={styles.email}><Link to={`/users/${item.user_id}`}>{item.email}</Link></td>}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            ) : (
                <p>Нет заказов</p>
            )}
        </div>
    )
}
