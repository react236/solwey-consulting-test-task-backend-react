/**
 * Профиль пользователя
 */
import { useParams, useNavigate, useLoaderData } from 'react-router-dom'
import { Access } from '../access/Access'

import { userRequest } from '../../api'
import { store } from '../../store/store'

import styles from './UserProfile.module.css'

export async function loader({ params }) {
    let user = store.getState().app['user']

    if (user.id !== Number(params['id'])) {
        if (user.role === 'admin') {
            user = await userRequest(params['id'])
        } else {
            return {
                error: 'Forbidden',
            }
        }
    }

    return { ... user };
}

export const UserProfile = () => {
    const user = useLoaderData()
    const params = useParams()
    const navigate = useNavigate()

    const handleEdit = () => {
        navigate(`/users/${params.id}/edit`, {
            replace: true
        })
    }

    const handleBack = () => {
        navigate(-1)
    }

    return (
        <div className={styles.container}>
            <p className={styles.name}><b>{user.first_name} {user.last_name}</b></p>
            <p className={styles.email}><b>{user.email}</b></p>
            <p className={styles.role}>Role: {user.role}</p>
            <Access role='user'>
                <button 
                    className={styles.button}
                    onClick={handleEdit}
                >Редактировать</button>
            </Access>
            <button
                className={styles.button}
                onClick={handleBack}
            >Назад</button>
        </div>
    )
}
