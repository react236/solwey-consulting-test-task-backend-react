import React, { useEffect } from 'react'
import { redirect, useSubmit } from 'react-router-dom'
import { store } from '../../store/store'
import { clearUser } from '../../store/actions'
import { logoutRequest } from '../../api'

export async function action({ request }) {
    const token = store.getState().app.user.token
    
    const data = await logoutRequest(token);
    store.dispatch(clearUser())

    return redirect(`/`);
}

export const Logout = () => {
    const submit = useSubmit()

    useEffect(() => {
        submit(null, { method: 'post', action: '/logout' })
    }, [submit])

    return (
        <div></div>
    )
}
