import React from 'react'
import { Form, redirect } from 'react-router-dom'

import { store } from '../../store/store'
import { storeUser, clearUsers } from '../../store/actions'
import { signupRequest, updateUserRequest } from '../../api'

import styles from './SignUp.module.css'

export async function action({ request, params }) {
    const formData = await request.formData()
    const email = formData.get('email')
    const password = formData.get('password')

    const data = await signupRequest(email, password);

    let token = ''
    if ('token' in data) {
        token = data.token
    }

    const data1 = await updateUserRequest(data.resource_owner.id, { 
        first_name: formData.get('first_name'),
        last_name: formData.get('last_name'),
        role: 'user',
    }, token)

    store.dispatch(storeUser({
        ...data1,
        token: data.token,
        refreshToken: data.refresh_token
    }))
    store.dispatch(clearUsers())
    return redirect(`/`);
}

export const SignUp = () => {
    return (
        <div className={styles.container}>
            <h1>Sign Up</h1>
            <Form method='post'>
                <div className={styles.form}>
                    <input
                        placeholder='First name'
                        aria-label='First Name'
                        type='text'
                        name='first_name'
                    />
                    <input
                        placeholder='Last name'
                        aria-label='Last Name'
                        type='text'
                        name='last_name'
                    />
                    <input
                        placeholder='email'
                        aria-label='email'
                        type='text'
                        name='email'
                    />
                    <input
                        placeholder='password'
                        aria-label='password'
                        type='password'
                        name='password'
                    />
                </div>
                <button className={styles.button} type='submit'>Sign Up</button>
            </Form>
        </div>
    )
}
