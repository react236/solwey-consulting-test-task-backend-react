import React, { ReactNode } from 'react'
import { Navigate, redirect } from 'react-router'
import { useAppSelector } from '../../store/hooks'

import { tRole } from '../../interfaces'

interface iProps {
    role: tRole
    to?: string
    children: ReactNode
}

export const Access: React.FC<iProps> = ({ role, to = '', children }) => {
    const currentRole = useAppSelector(store => store.app.user['role'])

    // Если роль подходит, то показываем children
    // Если не подходит, то не показываем или перенаправляем по указанному пути
    switch (role) {
        case '': {
            // Любая роль
            return <>{children}</>
        }
        case 'guest': {
            // Только незарегистрированные пользователи
            if (currentRole === '' || currentRole === 'guest') {
                return <>{children}</>
            }
            if (redirect) {
                return <Navigate to={to} replace />
            }
            return null
        }
        case 'user': {
            // Зарегистрированные пользователи или администраторы
            if (currentRole === 'user' || currentRole === 'admin') {
                return <>{children}</>
            }
            if (redirect) {
                return <Navigate to={to} replace />
            }
            return null
        }
        case 'admin': {
            // Только администраторы
            if (currentRole === 'admin') {
                return <>{children}</>
            }
            if (redirect) {
                return <Navigate to={to} replace />
            }
            return null
        }
        default: {
            return null
        }
    }
}
