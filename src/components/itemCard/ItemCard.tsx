/**
 * Карта товара
 */
import React from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'
import { Access } from '../access/Access'

import { itemsRequest } from '../../api'
import { store } from '../../store/store'
import { storeItems } from '../../store/actions'
import { format } from '../../utils'

import styles from './ItemCard.module.css'

export async function loader({ request }) {
    let items = store.getState().app['items']
    if (items.length === 0) {
        items = await itemsRequest('')
        store.dispatch(storeItems(items))
    }

    return { items };
}

export const ItemCard = () => {
    const items = useAppSelector(store => store.app['items'])
    const params = useParams()
    const navigate = useNavigate()

    const item = items.find((item) => item.id === Number(params.id))
    if ( !item ) {
        return <p>Товар не найден</p>
    }

    const handleEdit = () => {
        navigate(`/items/${params.id}/edit`, {
            replace: true
        })
    }

    const handleBack = () => {
        navigate(-1)
    }

    return (
        <div className={styles.container}>
            <p className={styles.name}><b>{item.name}</b></p>
            <span className={styles.price}><b>{format(item.price)}</b></span>
            <p className={styles.description}>{item.description}</p>
            <Access role='admin'>
                <button 
                    className={styles.button}
                    onClick={handleEdit}
                >Редактировать</button>
            </Access>
            <button
                className={styles.button}
                onClick={handleBack}
            >Назад</button>
        </div>
    )
}
