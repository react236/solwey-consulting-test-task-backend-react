import React from 'react'
import { Form, useNavigate, redirect } from 'react-router-dom'
import { newOrderRequest } from '../../api'
import { clearCart } from '../../store/actions'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { store } from '../../store/store'
import { format } from '../../utils'

import styles from './Cart.module.css'

export async function action({ request, params }) {
    const cart = store.getState().app['cart']
    
    const data = await newOrderRequest(cart);
    store.dispatch(clearCart())

    return redirect(`/orders`)
}

export const Cart = () => {
    const cart = useAppSelector(store => store.app['cart'])
    const dispatch = useAppDispatch()
    const itemsData = useAppSelector(store => store.app['items'])
    const navigate = useNavigate()

    let amount = 0 // Сумма итого

    const handleClear = () => {
        dispatch(clearCart())
        navigate('/')
    }

    return (
        <div className={styles.container}>
            <h1>Корзина</h1>
            {cart.length > 0 ? (
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Наименование</th>
                            <th>Количество</th>
                            <th>Цена</th>
                            <th>Сумма</th>
                        </tr>
                    </thead>
                    <tbody>
                        {cart.map((item) => {
                            const storedItem = itemsData.find((item2) => item2.id === item.id)
                            const sum = storedItem.price * item.count
                            amount = amount + sum
                            return (
                                <tr key={item.id}>
                                    <td className={styles.id}>{item.id}</td>
                                    <td className={styles.name}>{storedItem.name}</td>
                                    <td className={styles.count}>{item.count}</td>
                                    <td className={styles.price}>{format(storedItem.price)}</td>
                                    <td className={styles.sum}>{format(sum)}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th scope='row' className={styles.total}>Итого</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th className={styles.sum}>{format(amount)}</th>
                        </tr>
                    </tfoot>
                </table>
            ) : (
                <p>Корзина пуста</p>
            )}
            <Form method='post' id='pay'>
                {cart.length > 0 && (
                    <button className={styles.buttons} type='submit'>Оплатить</button>
                )}
                <button
                    className={styles.buttons}
                    type='button'
                    onClick={() => navigate(-1)}
                >Назад</button>
                {cart.length > 0 && (
                    <button
                        className={styles.buttons}
                        type='button'
                        onClick={handleClear}
                    >Очистить</button>
                )}
            </Form>
        </div>
    )
}
