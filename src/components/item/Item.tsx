/**
 * Виджет товара на главной
 */
import React from 'react'
import { Link } from 'react-router-dom'
import truncate from 'lodash.truncate'

import { ItemControl } from '../itemControl/ItemControl'
import { Access } from '../access/Access'
import { format } from '../../utils'

import styles from './Item.module.css'

interface iProps {
    id: number
    name: string
    description: string
    price: number
    inCart: boolean
}

export const Item: React.FC<iProps> = ({ id, name, description, price, inCart }) => {
    return (
        <div className={styles.container}>
            <Link to={`/items/${id}`} className={styles.link}>
                <p className={styles.name}><b>{truncate(name, { length: 55 })}</b></p>
                <span className={styles.price}><b>{format(price)}</b></span>
                <p className={styles.description}>{truncate(description, { length: 55 })}</p>
            </Link>
            <Access role='user'>
                <ItemControl id={id} inCart={inCart} />
            </Access>
        </div>
    )
}
