import React from 'react'

import { Link, useNavigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../store/hooks'

import { usersRequest, deleteUserRequest } from '../../api'
import { store } from '../../store/store'
import { storeUsers, deleteUser } from '../../store/actions'
import { iUser } from '../../interfaces'

import styles from './UsersList.module.css'
import { Access } from '../access/Access'

export async function loader({ request }) {
    let users = store.getState().app['users']
    if (users.length === 0 ) {
        users = await usersRequest()
        store.dispatch(storeUsers(users))
    }
    
    return { users };
}

export const UsersList = () => {
    const users = useAppSelector(store => store.app['users'])
    const dispatch = useAppDispatch()

    const handleDelete = async (id) => {
        await deleteUserRequest(id);
        // Удаляем пользователя со store
        dispatch(deleteUser(id))
    }

    return (
        <Access role='admin' to='/'>
            <div className={styles.container}>
                <h1>Список пользователей</h1>
                {users.length > 0 ? (
                    <table>
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Email</th>
                                <th>Роль</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.map((user: iUser) => {
                                return (
                                    <tr key={user.id}>
                                        <td className={styles.id}><Link to={`/users/${user.id}`}>{user.id}</Link></td>
                                        <td className={styles.name}><Link to={`/users/${user.id}`}>{user.first_name}</Link></td>
                                        <td className={styles.name}><Link to={`/users/${user.id}`}>{user.last_name}</Link></td>
                                        <td className={styles.email}><Link to={`/users/${user.id}`}>{user.email}</Link></td>
                                        <td className={styles.role}><Link to={`/users/${user.id}`}>{user.role}</Link></td>
                                        <td className={styles.delete} onClick={() => handleDelete(user.id)}>Удалить</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                ) : (
                    <p>Нет пользователей</p>
                )}
            </div>
        </Access>
    )
}
