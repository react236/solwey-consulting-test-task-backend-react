import LogoImage from '../../../assets/logo.svg'

export const Logo = () => {
    return (
        <LogoImage />
    )
}
