import { Link } from 'react-router-dom'
import { useAppSelector } from '../../../store/hooks'
import { Access } from '../../access/Access'

import { menuItems } from './menuItems'

import styles from './Menu.module.css'

export const Menu = () => {
    const userId = useAppSelector(store => store.app['user'].id)

    return (
        <ul className={styles.container}>
            {menuItems(userId).map((item) => {
                return (
                    <Access key={item.route} role={item.role}>
                        <li className={styles.item}>
                            <Link to={item.route}>{item.name}</Link>
                        </li>
                    </Access>
                )
            })}
        </ul>
    )
}
