import { tRole } from "../../../interfaces"

interface iMenuItems {
    name: string
    role: tRole
    route: string
}

export const menuItems = (id: number): iMenuItems[] => {
    return [
        {
            name: 'Каталог',
            role: '',
            route: '/',        
        },
        {
            name: 'Заказы',
            role: 'user',
            route: '/orders',        
        },
        {
            name: 'Профиль',
            role: 'user',
            route: `/users/${id}`,        
        },
        {
            name: 'Sign Up',
            role: 'guest',
            route: '/signup'
        },
        {
            name: 'Sign In',
            role: 'guest',
            route: '/signin'
        },
        {
            name: 'Товары',
            role: 'admin',
            route: '/items/list',        
        },
        {
            name: 'Пользователи',
            role: 'admin',
            route: '/users/list',        
        },
        {
            name: 'Выход',
            role: 'user',
            route: '/logout'
        },
    ]
}
