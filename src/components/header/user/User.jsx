import { useAppSelector } from '../../../store/hooks'

import styles from './User.module.css'

export const User = () => {
    const user = useAppSelector(store => store.app['user'])

    if (user.token === '') return null
    
    return (
        <span className={styles.container}>{user.email}</span>
    )
}
