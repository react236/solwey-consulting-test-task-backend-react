import { Menu } from './menu/Menu'
import { Logo } from './logo/Logo'
import { User } from './user/User'

import { CartBadge } from '../cartBadge/CartBadge'

import Cart from '../../assets/cart.svg'

import styles from './Header.module.css'
import { Link } from 'react-router-dom'

export const Header = () => {
    return (
        <div className={styles.container}>
            <div className={styles.left}>
                <Link to='/'><Logo /></Link>
                <Menu />
            </div>
            <div className={styles.right}>
                <User />
                <Link to='/cart'><Cart /></Link>
                <CartBadge />
            </div>
        </div>
    )
}
