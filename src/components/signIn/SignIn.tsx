import React from 'react'
import { Form, redirect } from 'react-router-dom'
import { store } from '../../store/store'
import { storeUser } from '../../store/actions'
import { signinRequest, userRequest } from '../../api'

import styles from './SignIn.module.css'

export async function action({ request }) {
    const formData = await request.formData()
    const email = formData.get('email')
    const password = formData.get('password')
    const data = await signinRequest(email, password);
    
    if ('token' in data) {
        const data1 = await userRequest(data.resource_owner.id, data.token)
        
        store.dispatch(storeUser({
            ...data1,
            token: data.token,
            refreshToken: data.refresh_token
        }))
    }
    return redirect(`/`);
}

export const SignIn = () => {
    return (
        <div className={styles.container}>
            <h1>Sign In</h1>
            <Form method='post'>
                <div className={styles.form}>
                    <input
                        placeholder='email'
                        aria-label='email'
                        type='text'
                        name='email'
                    />
                    <input
                        placeholder='password'
                        aria-label='password'
                        type='password'
                        name='password'
                    />
                </div>
                <button className={styles.button} type='submit'>Sign In</button>
            </Form>
        </div>
    )
}
